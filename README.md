Disclaimer: The information collected and presented in this project is for non-commercial aquaponic systems.



# Introduction

Who, what and why.

At the association Die Urbanisten e.V. in Dortmund, Germany we have been running aquaponic systems since 2013. The size of our systems are comparativiely small as they are home and community setups, operated with extensive to semi-intensive stocking densities. Since we are tinkerers we experimented with quite a spectrum of sensor and probe systems. This repository is an attempt to collect and make available all the experience we already generated and over time amend it with the experience of the aquaponics community.

The repository contains a code section for Arduinos, Raspberry Pis and other Debian/Ubuntu based computers. The aim is to offer a good starting point for setting up a measuring system. Most of the information presented herein can be collected from a vast array of sources on the net. This is the code that does the job for us.

## Translations

TOC:
Temperature
Humidity
pH
EC
DO
ORP
Ammonium
Nitite
Nitrate
Iron
Alkalinity
Moisture
Water levels
Water flows
Water volumes
Electricity

# What to measure, why, when and how


## Temperature

Why:
Relevant for:

plant growth
pollination
metabolism of the fish and food uptake
temperature swings stress fish
frost protection


In a small system with one sump tank for fishtank and grow beds the temperature differences between these subsystems is neglible.
If the subsystems are separated and heated, temperature measurement of the different parts can make sense.

How:

Easy to measure. 
Air temperature inside the greenhouse can be measured with a combined temperature and humidity probe. 
Outside air temperature and water temperatures with 1-wire (Dallas protocol) DS18B20 sensord. Can be bought in bulk which is cheaper than the wired versions. Wiring on a protoboard with 4.3kOhm pull-up (pull-down?) resistor. 


Where:

Fish tank temperature
Air temperature near and at the height of plants
Maximum air temperature at the to of the green house
Outside air temperature at a location without direct sunlight and out of the wind flow.

When:

A sample rate of one to five minutes is more than suffcient. Even 15 to 30 minute samples can be ok.


## Humidity

Why: 

In short: saturated air humidity can lead to condensate on the plants. Wet plant leaves are a good breeding ground for mold and fungi, which is why you should strive to keep the relative air humidity well below 90% if possible. If the air humidity is too low, plants evaporate more water, stressing the plants in extreme situations and leading to higher water consumption in your system. In extreme cases with continued high temperatures and low humidity plants develop more root mass in order to uptake and evaporate more water. This effort of the plant does not go into the crop and fruit production, lowering your yield and might even clog up the grow bed, leading to aeration and spillage problems.

The relative humidity of the air is a measurement for how much water is already contained in the air and how much more it could take up. The amount of water that can be taken up by air is very dependent on air temperature. This is the reason why the amount of water in air is not given in absolute terms but in relation to how much water the air could hold before condensation occurs a the given temperature. It is the relaive humidity, relative to the maximum at the current temperature. This means that this measurement will show swings in direct correlation to temperature swings. Calculations on water contained in air are mostly done with the help of the Mollier diagram. Note that x axis dimension is "gram of water in grams of dry air". The dimension might seem a little strange at first, but since normally the amount of dry air in the observed system is constant, calculations on water in air become much easier.

TODO: Insert Wikipedia link to mollier diagram

How:

Sensors for humidity are usually combined with an air temperature sensor. Accuracy differs from sensor to sensor, but usually the simple and less accurate should still be suffcient for a simple aquaponic or hydroponic system. Price difference for more accurate sensors is not very steep though, so choose as you like.

TODO: Insert links to different sensor offerings from adafruit and such

Where:

Since there might be a temperature gradient within your growing area, place the sensor in a well ventilated place near the plants or at least at the height of the plants. Shiled from direct sunlight. If you are using a fan, then place the sensor in the air flow of the fan, but not too close to it to avoid wrong measurements due to the wind chill effect.

When:

A sample rate of one to five minutes is more than suffcient. Even 15 to 30 minute samples can be ok.
When controlling fans or windows, a shorter sample rate should be considered to avoid oscillation.

## pH

Why:

How:

Where:

When:

## EC


Why:

How:

Where:

When:

## DO

Why:

How:

Where:

When:

## ORP

Why:

How:

Where:

When:

## Ammonium

Why:

How:

Where:

When:

## Nitrite

Why:

How:

Where:

When:

## Nitrate

Why:

How:

Where:

When:

## Iron

Why:

How:

Where:

When:

## Alkalinity

Why:

How:

Where:

When:

## Moisture

Why:

How:

Where:

When:

## Water levels

Why:

How:

Where:

When:

## Water flows

Why:

How:

Where:

When:

## Water volumes

Why:
Total water consumption

How:
Water meter with tick
mbus

Where:


When:

## Electricity

Why:

How:

Where:

When:


# Topology

## Measuring, monitoring, notifications and alerts and controlling

Detail need for differentiating between monitoring and controling

## Arduino, Raspberry Pi, VPS and data collecting services

### Local display

16x2 rgb display

### Data processing and collection

Node Red
InfluxDB

### Dashboard

Freeboard
Grafana

### Services

Xively
Grovestreams

## Sensors

### Ecosystems

Libellium
Break out boards from Adafruit and Sparkfun
Seeedstudio Grove

### Temperature and humidity

### Temperature

### pH, EC, DO and ORP

#### Measurement collecting systems

Libellium Smart Water
Cooking hacks OpenAquarium and OpenAquaponics
Atlas Scientific
Sparky's Widgets

#### pH probe degradation 

long term measurement
quality of probes
protein cluttering


### Ammonium, Nitrite, nitrate and other ions

### Light

## Actors

### Automatic feeder
